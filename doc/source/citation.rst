Citation for |molcas|
=====================

The recommended citations for |molcas| Version |molcasversion| are:

**Molcas 8:** F. Aquilante, J. Autschbach, R. K. Carlson, L. F. Chibotaru,
M. G. Delcey, L. De Vico, I. Fdez. Galván, N. Ferré, L. M. Frutos, L. Gagliardi,
M. Garavelli, A. Giussani, C. E. Hoyer, G. Li Manni, H. Lischka, D. Ma, P. Å. Malmqvist,
T. Müller, A. Nenov, M. Olivucci, T. B. Pedersen, D. Peng, F. Plasser, B. Pritchard,
M. Reiher, I. Rivalta, I. Schapiro, J. Segarra-Martí, M. Stenrup, D. G. Truhlar,
L. Ungur, A. Valentini, S. Vancoillie, V. Veryazov, V. P. Vysotskiy, O. Weingart,
F. Zapata, R. Lindh, *Journal of Computational Chemistry*, **37**, 506 (2016).
DOI: `10.1002/jcc.24221 <https://doi.org/10.1002/jcc.24221>`_

**Cholesky infrastructure:** F. Aquilante, T. B. Pedersen, V. Veryazov, R. Lindh,
*Wiley Interdisciplinary Reviews: Computational Molecular Science*, **3**, 143 (2013).
DOI: `10.1002/wcms.1117 <https://doi.org/10.1002/wcms.1117>`_

**Molcas 7:** F. Aquilante, L. De Vico, N. Ferré, G. Ghigo,
P.-Å. Malmqvist, P. Neogrády, T. B. Pedersen, M. Pitoňák, M. Reiher,
B. O. Roos, L. Serrano-Andrés, M. Urban, V. Veryazov, R. Lindh,
*Journal of Computational Chemistry*, **31**, 224 (2010).
DOI: `10.1002/jcc.21318 <https://doi.org/10.1002/jcc.21318>`_

**Code development:** V. Veryazov, P.-O. Widmark, L. Serrano-Andrés, R. Lindh, B. O. Roos,
*International Journal of Quantum Chemistry*, **100**, 626 (2004).
DOI: `10.1002/qua.20166 <https://doi.org/10.1002/qua.20166>`_

**Molcas 6:** G. Karlström, R. Lindh, P.-Å. Malmqvist, B. O. Roos, U. Ryde,
V. Veryazov, P.-O. Widmark, M. Cossi, B. Schimmelpfennig, P. Neogrády, L. Seijo,
*Computational Material Science*, **28**, 222 (2003).
DOI: `10.1016/S0927-0256(03)00109-5 <https://doi.org/10.1016/S0927-0256(03)00109-5>`_

The following persons have contributed to the development of the
|molcas| software:

.. In alphabetical order (treating Å as A, etc.)

Kerstin Andersson,
Francesco Aquilante,
Maria Barysz,
Anders Bernhardsson,
Margareta R. A. Blomberg,
Jonas Boström,
Yannick Carissan,
Liviu Chibotaru,
David L. Cooper,
Maurizio Cossi,
Oleh Danyliv,
Mickaël G. Delcey,
Ajitha Devarajan,
Luca De Vico,
Ignacio Fdez. Galván,
Nicolas Ferré,
Markus P. Fülscher,
Alexander Gaenko,
Laura Gagliardi,
Giovanni Ghigo,
Coen de Graaf,
Sergey Gusarov,
Daniel Hagberg,
Jun-ya Hasegawa,
José Manuel Hermida-Ramón,
Bernd Artur Heß,
Asbjørn Holt,
Gunnar Karlström,
Jesper Wisborg Krogh,
Roland Lindh,
Giovanni Li Manni,
Per Åke Malmqvist,
Takahito Nakajima,
Pavel Neogrády,
Anders Öhrn,
Jeppe Olsen,
Thomas Bondo Pedersen,
Daoling Peng,
Michal Pitoňák,
Juraj Raab,
Markus Reiher,
Björn O. Roos,
Ulf Ryde,
Igor Schapiro,
Bernd Schimmelpfennig,
Martin Schütz,
Luis Seijo,
Luis Serrano-Andrés,
Per E. M. Siegbahn,
Pär Söderhjelm,
Jonna Stålring,
Bingbing Suo,
Peter V. Sushko,
Thorstein Thorsteinsson,
Takashi Tsuchiya,
Liviu Ungur,
Steven Vancoillie,
Valera Veryazov,
Victor P. Vysotskiy,
Ulf Wahlgren,
Per-Olof Widmark.
